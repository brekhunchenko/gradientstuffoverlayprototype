//
//  GradientStuffOverlayView.h
//  GradientStuffOverlayPrototype
//
//  Created by Yaroslav Brekhunchenko on 3/31/19.
//  Copyright © 2019 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GradientStuffOverlayView : UIView

@property (nonatomic, strong) UIColor* overlayColor;

- (void)removeAllClearRectangles;
- (void)appendClearAreaRectangle:(CGRect)rect;

@end

@interface GradientStuffLayer : CAGradientLayer

@end

NS_ASSUME_NONNULL_END
