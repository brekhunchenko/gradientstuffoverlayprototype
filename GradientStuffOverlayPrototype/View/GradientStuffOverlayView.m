//
//  GradientStuffOverlayView.m
//  GradientStuffOverlayPrototype
//
//  Created by Yaroslav Brekhunchenko on 3/31/19.
//  Copyright © 2019 Yaroslav Brekhunchenko. All rights reserved.
//

#import "GradientStuffOverlayView.h"

@interface GradientStuffOverlayView()

@property (nonatomic, strong) NSMutableArray<GradientStuffLayer *>* gradientStuffLayers;
@property (nonatomic, strong) NSMutableArray<CAShapeLayer *>* emptyAreaLayers;
@property (nonatomic, strong) NSMutableArray<NSValue *>* clearAreasRectangles;

@end

@implementation GradientStuffOverlayView

#pragma mark - Overridden

- (void)awakeFromNib {
  [super awakeFromNib];
  
  self.backgroundColor = [UIColor clearColor];
  self.overlayColor = [[UIColor blueColor] colorWithAlphaComponent:0.5];
  
  self.gradientStuffLayers = [NSMutableArray new];
  self.clearAreasRectangles = [NSMutableArray new];
  self.emptyAreaLayers = [NSMutableArray new];
}

#pragma mark - Class Methods

- (void)appendClearAreaRectangle:(CGRect)clearAreaRectangle {
  if (clearAreaRectangle.origin.x + clearAreaRectangle.size.width > self.bounds.size.width) {
    NSLog(@"Error. Invalid clear area rectangle frame.");
    return;
  }
  
  CGFloat minThresholdForGradient = 20.0;
  if (clearAreaRectangle.origin.x > minThresholdForGradient) {
    CGRect leftPartGradientRectangle = CGRectMake(0.0,
                                                  clearAreaRectangle.origin.y,
                                                  clearAreaRectangle.origin.x,
                                                  clearAreaRectangle.size.height);
    GradientStuffLayer* leftPartGradient = [self _stuffGradientLayer:leftPartGradientRectangle isLeftPart:YES];
    [self.layer insertSublayer:leftPartGradient atIndex:0];
    [self.gradientStuffLayers addObject:leftPartGradient];
  }
  
  if (CGRectGetMaxX(clearAreaRectangle) + minThresholdForGradient < self.bounds.size.width) {
    CGRect rightPartGradientRectangle = CGRectMake(CGRectGetMaxX(clearAreaRectangle),
                                                   clearAreaRectangle.origin.y,
                                                   self.bounds.size.width - CGRectGetMaxX(clearAreaRectangle),
                                                   clearAreaRectangle.size.height);
    GradientStuffLayer* rightPartGradient = [self _stuffGradientLayer:rightPartGradientRectangle isLeftPart:NO];
    [self.layer insertSublayer:rightPartGradient atIndex:0];
    [self.gradientStuffLayers addObject:rightPartGradient];
  }
  
  [self.clearAreasRectangles addObject:[NSValue valueWithCGRect:clearAreaRectangle]];
  
  [self _removeAllEmptyLayerOverlays];
  for (NSValue* emptyAreaRectValue in [self _calculateEmptyAreaOverlays]) {
    CAShapeLayer* emptyLayerOverlay = [self _emptyAreaOverlay:emptyAreaRectValue.CGRectValue];
    [self.layer insertSublayer:emptyLayerOverlay atIndex:0];
    [self.emptyAreaLayers addObject:emptyLayerOverlay];
  }
}

- (void)removeAllClearRectangles {
  for (GradientStuffLayer* gradienLayer in _gradientStuffLayers) {
    [gradienLayer removeFromSuperlayer];
  }
  self.gradientStuffLayers = [NSMutableArray new];
  self.clearAreasRectangles = [NSMutableArray new];
  [self _removeAllEmptyLayerOverlays];
}

#pragma mark - Utilz

- (GradientStuffLayer *)_stuffGradientLayer:(CGRect)frame isLeftPart:(BOOL)isLeftPart {
  GradientStuffLayer *gradient = [GradientStuffLayer layer];
  gradient.frame = frame;
  gradient.startPoint = CGPointMake(1.0, 0.0);
  gradient.endPoint = CGPointMake(0.0, 0.0);
  gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor clearColor] CGColor], (id)[self.overlayColor CGColor], nil];
  gradient.masksToBounds = YES;
  CGFloat gradientStopPoints = 20.0f;
  NSNumber* gradientStop = @(gradientStopPoints/frame.size.width);
  gradient.locations = @[@0.0, gradientStop, @1.0];
  gradient.backgroundColor = [[UIColor clearColor] CGColor];
  if (!isLeftPart) {
    gradient.transform = CATransform3DMakeScale(-1.0, 1.0, 1.0);
  }
  return gradient;
}

- (NSArray<NSValue *> *)_calculateEmptyAreaOverlays {
  if ([_clearAreasRectangles count] == 0) {
    return @[];
  }
  NSMutableArray<NSValue *>* emptyAreasOverlayRectangles = [NSMutableArray new];
  CGRect clearAreaRect = [_clearAreasRectangles.firstObject CGRectValue];
  [emptyAreasOverlayRectangles addObject:[NSValue valueWithCGRect:CGRectMake(0.0, 0.0, self.bounds.size.width, clearAreaRect.origin.y)]];
  CGRect lastClearAreaRect = [_clearAreasRectangles.lastObject CGRectValue];
  [emptyAreasOverlayRectangles addObject:[NSValue valueWithCGRect:CGRectMake(0.0,
                                                                             CGRectGetMaxY(lastClearAreaRect),
                                                                             self.bounds.size.width,
                                                                             self.bounds.size.height - CGRectGetMaxY(lastClearAreaRect))]];
  return emptyAreasOverlayRectangles;
}

- (CAShapeLayer *)_emptyAreaOverlay:(CGRect)frame {
  CAShapeLayer* layer = [CAShapeLayer layer];
  layer.frame = frame;
  layer.backgroundColor = [self.overlayColor CGColor];
  return layer;
}

- (void)_removeAllEmptyLayerOverlays {
  for (CAShapeLayer* emptyLayerOverlay in _emptyAreaLayers) {
    [emptyLayerOverlay removeFromSuperlayer];
  }
  self.emptyAreaLayers = [NSMutableArray new];
}

@end

@implementation GradientStuffLayer

@end
