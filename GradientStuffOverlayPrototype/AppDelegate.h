//
//  AppDelegate.h
//  GradientStuffOverlayPrototype
//
//  Created by Yaroslav Brekhunchenko on 3/31/19.
//  Copyright © 2019 Yaroslav Brekhunchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

