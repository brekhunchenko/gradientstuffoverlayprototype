//
//  ViewController.m
//  GradientStuffOverlayPrototype
//
//  Created by Yaroslav Brekhunchenko on 3/31/19.
//  Copyright © 2019 Yaroslav Brekhunchenko. All rights reserved.
//

#import "ViewController.h"
#import "View/GradientStuffOverlayView.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet GradientStuffOverlayView *gradientStuffOverlayView;

@end

@implementation ViewController

#pragma mark - UIViewController Life Cycle

- (void)viewDidLoad {
  [super viewDidLoad];
}

#pragma mark - Actions

- (IBAction)addGradientEffectButtonAction:(id)sender {
  [_gradientStuffOverlayView removeAllClearRectangles];
  
  CGFloat stuffHeight = 60.0;
  CGRect clearAreaRectangle0 = CGRectMake(150.0, 400.0, 180.0, stuffHeight);
  [_gradientStuffOverlayView appendClearAreaRectangle:clearAreaRectangle0];
 
  CGRect clearAreaRectangle1 = CGRectMake(0.0, 460.0, self.view.bounds.size.width, stuffHeight);
  [_gradientStuffOverlayView appendClearAreaRectangle:clearAreaRectangle1];
  
  CGRect clearAreaRectangle2 = CGRectMake(0.0, 520.0, 200.0, stuffHeight);
  [_gradientStuffOverlayView appendClearAreaRectangle:clearAreaRectangle2];
}

@end
